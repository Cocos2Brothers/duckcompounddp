#include "CountingDuckFactory.h"

#include "Ducks.h"
#include "QuackCounter.h"



CountingDuckFactory::CountingDuckFactory()
{
}


CountingDuckFactory::~CountingDuckFactory()
{
}

Quackable * CountingDuckFactory::createMallardDuck()
{
    return new QuackCounter ( new MallardDuck ) ;
}

Quackable * CountingDuckFactory::createRedheadDuck()
{
    return new QuackCounter( new RedheadDuck );
}

Quackable * CountingDuckFactory::createDuckCall()
{
    return new QuackCounter( new DuckCall );
}

Quackable * CountingDuckFactory::createRubberDuck()
{
    return new QuackCounter( new RubberDuck );
}
