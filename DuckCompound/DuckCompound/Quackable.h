#pragma once

#include "QuackObservable.h"


class Quackable: public QuackObservable
{

public:

    Quackable() ;
    virtual ~Quackable() ;

    virtual void quack() = 0;




};
