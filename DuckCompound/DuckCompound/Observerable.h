#pragma once

#include <vector>

#include "QuackObservable.h"

class Observer;

class Observerable :
    public QuackObservable
{
    QuackObservable * duck;
    std::vector < Observer * > observers; 
public:
    Observerable(QuackObservable * duckPar);
    virtual ~Observerable();

    virtual void registerObserver( Observer * observer );
    virtual void notifyObservers();
};

