#pragma once

#include "AbstrackDuckFactory.h"


class DuckFactory :
    public AbstrackDuckFactory
{
public:
    DuckFactory();
    virtual ~DuckFactory();

    virtual  Quackable * createMallardDuck();
    virtual  Quackable * createRedheadDuck() ;
    virtual  Quackable * createDuckCall();
    virtual  Quackable * createRubberDuck();


};

