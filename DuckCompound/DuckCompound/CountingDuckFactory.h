#pragma once
#include "AbstrackDuckFactory.h"


class CountingDuckFactory :
    public AbstrackDuckFactory
{
public:
    CountingDuckFactory();
    virtual ~CountingDuckFactory();

    virtual  Quackable * createMallardDuck();
    virtual  Quackable * createRedheadDuck();
    virtual  Quackable * createDuckCall();
    virtual  Quackable * createRubberDuck();


};

