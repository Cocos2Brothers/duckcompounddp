#pragma once
#include "Quackable.h"

//decorator
class QuackCounter: public Quackable
{
    Quackable * duck; 
    static int duckCounter;

public:
 
    QuackCounter( Quackable * duckPar );

    virtual ~QuackCounter();

    virtual void quack() override;

    static int getQuackCounter();

    virtual void registerObserver( Observer * observer );
    void notifyObservers();
};

