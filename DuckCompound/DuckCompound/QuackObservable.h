#pragma once

#include "Observer.h"


class QuackObservable
{
public:
    virtual void registerObserver( Observer * observer ) = 0; 
    virtual void notifyObservers() = 0; 

};

