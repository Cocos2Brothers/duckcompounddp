#include "Quackologist.h"

#include <iostream>

void Quackologist::update( QuackObservable * duck )
{
    std::cout << " Quackologist " << duck << " just quackaked " << std::endl;
}
