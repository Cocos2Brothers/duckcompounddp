#include "DuckFactory.h"
#include "Ducks.h"



DuckFactory::DuckFactory()
{
}


DuckFactory::~DuckFactory()
{
}

Quackable * DuckFactory::createMallardDuck()
{
    return new MallardDuck;
}

Quackable * DuckFactory::createRedheadDuck()
{
    return new RedheadDuck;
}

Quackable * DuckFactory::createDuckCall()
{
    return new DuckCall;
}

Quackable * DuckFactory::createRubberDuck()
{
    return new RubberDuck;
}
