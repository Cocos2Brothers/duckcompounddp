#include "Flock.h"

#include "Observerable.h"
#include <iostream>

Flock::Flock()
{
    this->observable = new Observerable( this );
}


Flock::~Flock()
{
    delete observable;
}

void Flock::add( Quackable * quacker )
{
    quackers.push_back( quacker );
}

void Flock::quack()
{
    auto it = quackers.begin(); 
    for ( ; it < quackers.end(); ++it )
    {
        ( *it )->quack(); 
    }

}

void Flock::registerObserver( Observer * observer )
{
    this->observable->registerObserver( observer );
    auto it = quackers.begin();
    for ( ; it < quackers.end(); ++it )
    {
        ( *it )->registerObserver( observer );
    }
}

void Flock::notifyObservers()
{
    std::cout << " flock notify " << this << std::endl;
    this->observable->notifyObservers();
}
