#pragma once
#include <iostream>

#include "Quackable.h"
#include "Observerable.h"

class MallardDuck : public Quackable
{
    Observerable *  observable;
public:
    MallardDuck();
    virtual ~MallardDuck();

    void quack() override;

    virtual void registerObserver( Observer * observer );
    void notifyObservers();
};

class RedheadDuck : public Quackable
{
    Observerable *  observable;
public:
    RedheadDuck();
    virtual ~RedheadDuck();

    void quack() override;

    virtual void registerObserver( Observer * observer );
    void notifyObservers();

   
};

class DuckCall : public Quackable
{
    Observerable *  observable;
public:
    DuckCall();
    virtual ~DuckCall();

    void quack() override;

    virtual void registerObserver( Observer * observer );
    void notifyObservers();

};

class RubberDuck : public Quackable
{
    Observerable *  observable;
public:
    RubberDuck();
    virtual ~RubberDuck();

    void quack() override;

    virtual void registerObserver( Observer * observer );
    void notifyObservers();

};
