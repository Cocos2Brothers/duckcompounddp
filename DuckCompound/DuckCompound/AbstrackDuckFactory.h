#pragma once
#include "Quackable.h"

class AbstrackDuckFactory
{
public:
    virtual  Quackable * createMallardDuck() = 0;
    virtual  Quackable * createRedheadDuck() = 0;
    virtual  Quackable * createDuckCall() = 0;
    virtual  Quackable * createRubberDuck() = 0;

};

