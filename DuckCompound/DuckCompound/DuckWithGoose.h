#pragma once
#include "DuckSimulator.h"
class DuckWithGoose :
    public DuckSimulator
{
public:
    DuckWithGoose();
    virtual ~DuckWithGoose();
    Quackable * createGooseWithAdapter();
};

