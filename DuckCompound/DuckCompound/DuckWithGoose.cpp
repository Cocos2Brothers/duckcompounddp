#include "DuckWithGoose.h"

#include "Goose.h"

DuckWithGoose::DuckWithGoose()
{
}


DuckWithGoose::~DuckWithGoose()
{
}


Quackable * DuckWithGoose::createGooseWithAdapter() 
{
    return new GooseAdapter( new Goose() );
}