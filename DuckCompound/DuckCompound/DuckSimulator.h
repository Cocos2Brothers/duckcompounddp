#pragma once

#include "Quackable.h"
#include "AbstrackDuckFactory.h"


class DuckSimulator
{
public:
    void simulate( AbstrackDuckFactory * duckFactory );

    void simulate( Quackable * duck );

    virtual Quackable * createGooseWithAdapter() = 0;

};
