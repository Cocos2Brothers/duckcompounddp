#include <iostream>

#include "DuckWithGoose.h"
#include "AbstrackDuckFactory.h"
#include "CountingDuckFactory.h"
#include "DuckFactory.h"


int main()
{
    DuckSimulator * duckSimulator = new DuckWithGoose();
    AbstrackDuckFactory * duckFactory = new CountingDuckFactory;
    //AbstrackDuckFactory * duckFactory = new DuckFactory;


    duckSimulator->simulate(duckFactory);

    system( "pause" );
    return 0;
}