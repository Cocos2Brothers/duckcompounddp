#include "Observerable.h"

#include <iostream>

Observerable::Observerable( QuackObservable * duckPar ): duck (duckPar)
{
}


Observerable::~Observerable()
{
}

void Observerable::registerObserver( Observer * observer )
{
    observers.push_back( observer );
}

void Observerable::notifyObservers()
{
    auto it = observers.begin();
    for ( ; it != observers.end(); ++it ) {
        ( *it )->update( duck );
    }
}
