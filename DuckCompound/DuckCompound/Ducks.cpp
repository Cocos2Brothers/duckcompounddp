#pragma once 

#include "Ducks.h"

#include "Observerable.h"


MallardDuck::MallardDuck()
{
    this->observable = new Observerable( this );
}

MallardDuck::~MallardDuck()
{
    delete observable;
}

void MallardDuck::registerObserver( Observer * observer )
{
    observable->registerObserver( observer );
}

void MallardDuck::notifyObservers()
{
    observable->notifyObservers();
}

void MallardDuck::quack() 
{
    std::cout << " Mallardock Quack " << std::endl;
    notifyObservers();
}


RedheadDuck::RedheadDuck()
{
    this->observable = new Observerable( this );
}

RedheadDuck::~RedheadDuck()
{
    delete observable;
}

void RedheadDuck::registerObserver( Observer * observer )
{
    observable->registerObserver( observer );
}

void RedheadDuck::notifyObservers()
{
    observable->notifyObservers();
}

void RedheadDuck::quack()
{
    std::cout << " RedheadDuck Quack " << std::endl;
    notifyObservers();
}

DuckCall::DuckCall()
{
    this->observable = new Observerable( this );
}

DuckCall::~DuckCall()
{
    delete observable;
}

void DuckCall::registerObserver( Observer * observer )
{
    observable->registerObserver( observer );
}

void DuckCall::notifyObservers()
{
    observable->notifyObservers();
}

void DuckCall::quack()
{
    std::cout << " DuckCall Quack " << std::endl;
    notifyObservers();
}


RubberDuck::RubberDuck()
{
    this->observable = new Observerable( this );
}

RubberDuck::~RubberDuck()
{
    delete observable;
}

void RubberDuck::registerObserver( Observer * observer )
{
    observable->registerObserver( observer );
}

void RubberDuck::notifyObservers()
{
    observable->notifyObservers();
}

void RubberDuck::quack()
{
    std::cout << " RubberDuck Quack " << std::endl;
    notifyObservers();
}