#pragma once

#include <vector>

#include "Quackable.h"
class Observerable;

class Flock :
    public Quackable
{
    Observerable * observable;
    std::vector < Quackable * > quackers; 

public:
    Flock();
    virtual ~Flock();
    
    virtual void add ( Quackable * quacker ); 
    virtual void quack();
    void registerObserver( Observer * observer );
    void notifyObservers();
};

