#pragma once
#include "Observer.h"
class Quackologist :
    public Observer
{
public:

    virtual void update( QuackObservable * duck );

};

