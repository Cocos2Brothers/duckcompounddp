#include "DuckSimulator.h"

#include "Ducks.h"
#include "QuackCounter.h"
#include "Flock.h"
#include "Quackologist.h"



void DuckSimulator::simulate(AbstrackDuckFactory * duckFactory)
{
    Quackable * mallardDuck = duckFactory->createMallardDuck();
    Quackable * redheadDuck = duckFactory->createRedheadDuck();
    Quackable * duckCall = duckFactory->createDuckCall();
    Quackable * rubberDuck = duckFactory->createRubberDuck();

    Quackable * goose = this->createGooseWithAdapter();

    std::cout << " Duck Simulator " << std::endl;

    Flock * flockOfDucks = new Flock;
    flockOfDucks->add( mallardDuck );
    flockOfDucks->add( redheadDuck );
    flockOfDucks->add( duckCall );
    flockOfDucks->add( rubberDuck );


    Quackable * mallardDuck2 = duckFactory->createMallardDuck();
    Quackable * mallardDuck3 = duckFactory->createMallardDuck();
    Quackable * mallardDuck4 = duckFactory->createMallardDuck();
    Quackable * mallardDuck5 = duckFactory->createMallardDuck();

    Flock * flockOfMallards = new Flock;
    flockOfMallards->add( mallardDuck2 );
    flockOfMallards->add( mallardDuck3 );
    flockOfMallards->add( mallardDuck4 );
    flockOfMallards->add( mallardDuck5 );


    flockOfDucks->add( flockOfMallards );

    Quackologist * quackLogist = new Quackologist;

    flockOfDucks->registerObserver( quackLogist );

    std::cout << " Entire Flock of Ducks " << std::endl;
    simulate( flockOfDucks );
    std::cout << " flock of Mallards " << std::endl;
    simulate( flockOfMallards );
    std::cout << " one simple mallar duck " << std::endl;
    simulate( mallardDuck );

    std::cout << "Duck Counter : " << QuackCounter::getQuackCounter() << std::endl;
}

void DuckSimulator::simulate( Quackable * duck )
{
    duck->quack();
}

