#pragma once
#include <iostream>
#include "Quackable.h"

#include "Observerable.h"


class Goose
{
public:


    virtual void honk()
    {
        std::cout << " Honk " << std::endl; 
    }
};


class GooseAdapter : public Quackable
{

    Goose * goose;
    Observerable * observable;

public:
    GooseAdapter( Goose * goosePar ): goose( goosePar )
    {
        this->observable = new Observerable(this);
    }

    GooseAdapter() 
    {
        delete observable;
    }

    virtual void quack()
    {
        goose->honk();
    }

    void registerObserver( Observer * observer )
    {
        observable->registerObserver( observer );
    }

    void notifyObservers()
    {
        observable->notifyObservers();
    }

};