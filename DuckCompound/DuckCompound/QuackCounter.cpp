#include "QuackCounter.h"


QuackCounter::QuackCounter( Quackable * duckPar ) : duck( duckPar )
{
}

QuackCounter::~QuackCounter()
{
}

void QuackCounter::quack()
{
    duck->quack();
    duckCounter++;
}

int QuackCounter::getQuackCounter()
{
    return duckCounter;
}

int QuackCounter::duckCounter = 0;

void QuackCounter::registerObserver( Observer * observer )
{
    duck->registerObserver( observer );
}

void QuackCounter::notifyObservers()
{
    duck->notifyObservers();
}